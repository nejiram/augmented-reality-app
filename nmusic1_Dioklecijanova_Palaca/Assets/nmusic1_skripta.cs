﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.Audio;
using UnityEngine.UI;

public class nmusic1_skripta : MonoBehaviour, ITrackableEventHandler {

    Ray ray;
    RaycastHit hit;
    TrackableBehaviour mTrackableBehaviour;

    public GameObject soundOn;
    public GameObject soundOff;
    public GameObject photo_album;
    public GameObject photo;
    public GameObject map;
    public GameObject infoText;
    public GameObject helpText;
    public string url;
    

    // Use this for initialization
    void Start () {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
        }
    }



    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            //Play audio when target is found
            OnTrackingFound();
        }
        else {
            //Stop audio when target is lost
            OnTrackingLost();
        }
    }
	
    protected virtual void OnTrackingFound()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        //Enable rendering
        foreach (var component in rendererComponents)
            component.enabled = true;

        //Enable colliders
        foreach (var component in colliderComponents)
            component.enabled = true;

        //Enable canvas
        foreach (var component in canvasComponents)
            component.enabled = true;

        if (mTrackableBehaviour.gameObject.GetComponentInChildren<AudioSource>() != null)
        {
            mTrackableBehaviour.gameObject.GetComponentInChildren<AudioSource>().Play();
        }
    }

    protected virtual void OnTrackingLost()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = false;

        // Disable canvas:
        foreach (var component in canvasComponents)
            component.enabled = false;
        // added

        if (mTrackableBehaviour.gameObject.GetComponentInChildren<AudioSource>() != null)
        {
            mTrackableBehaviour.gameObject.GetComponentInChildren<AudioSource>().Pause();
        }
    }

	// Update is called once per frame
	void Update () {

        ray = Camera.main.ScreenPointToRay(Input.mousePosition); //salje se zraka od mjesta gdje je dodirnut ekran

        
        if (Physics.Raycast(ray, out hit)) //provjerava da li je pogodjen neki objekat i upisuje ga u hit ako jeste
        {
            if (hit.collider.gameObject.name == "nmusic1_sound_on")
            {
                soundOn.SetActive(false);
                soundOff.SetActive(true);
                mTrackableBehaviour.gameObject.GetComponentInChildren<AudioSource>().Play();
            }
            if (hit.collider.gameObject.name == "nmusic1_sound_off")
            {
                soundOff.SetActive(false);
                soundOn.SetActive(true);
                mTrackableBehaviour.gameObject.GetComponentInChildren<AudioSource>().Stop();
            }
            if (hit.collider.gameObject.name == "nmusic1_photo_album")
            {
                photo_album.SetActive(false);
                photo.SetActive(true);
            }
            if (hit.collider.gameObject.name == "nmusic1_image")
            {
                photo.SetActive(false);
                photo_album.SetActive(true);
            }
            if (hit.collider.gameObject.name == "nmusic1_map")
            {
                Application.OpenURL(url);
            }
        }
		
	}

    public void Exit()
    {
        Application.Quit();
    }

    public void ShowInfoText()
    {
        if (infoText.activeInHierarchy == true)
            infoText.SetActive(false);
        else
            infoText.SetActive(true);
    }

    public void ShowHelpText()
    {
        if (helpText.activeInHierarchy == true)
            helpText.SetActive(false);
        else
            helpText.SetActive(true);
    }

}
